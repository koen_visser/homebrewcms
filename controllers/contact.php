<?php
/**
 * Contact.php - Controller for contact. Adds messages to the contact table
 * 
 * @author Bugslayer
 * 
 */
// Include the tools
require_once dirname ( __FILE__ ).'/../components/db.php';
include_once dirname ( __FILE__ ).'/../components/datatools.php';
include_once dirname ( __FILE__ ).'/../components/formvalidationtools.php';

global $action; // set in index.php

// Determine and process the action.
switch ($action) {
	case "save" :
		send_message();
		header ( "Location: index.php" );
		break;
	default:
		die ( "Illegal action" );
}

function send_message() {
	global $mysqli;
	// Check required fields
	if (! isset ( $_POST ['Name_first'] ) || ! isset ( $_POST ['Name_middle'] ) || ! isset ( $_POST ['Name_last'] ) 
			|| ! isset ( $_POST ['Email'] ) || ! isset ( $_POST ['Telephone'] ) || ! isset ( $_POST ['Address_street'] ) 
			|| ! isset ( $_POST ['Address_number'] ) || ! isset ( $_POST ['Address_zipcode'] ) || ! isset ( $_POST ['Address_city'] ) 
			|| ! isset ( $_POST ['Subject'] ) || ! isset ( $_POST ['Message'] )) {
		die ( 'Error in form' );
	}
	
	// Secure the post data and assign it to local variables
	$name_first = strip_tags ( $_POST ['Name_first'] );
	$name_middle = strip_tags ( $_POST ['Name_middle'] );
	$name_last = strip_tags ( $_POST ['Name_last'] );
	$phone = strip_tags ( $_POST ['Telephone'] );
	$email = strip_tags ( $_POST ['Email'] );
	$address_street = strip_tags ( $_POST ['Address_street'] );
	$address_number = strip_tags ( $_POST ['Address_number'] );
	$address_zipcode = strip_tags ( $_POST ['Address_zipcode'] );
	$address_city = strip_tags ( $_POST ['Address_city'] );
	$subject = strip_tags ( $_POST ['Subject'] );
	$message = strip_tags ( $_POST ['Message'] );
	
	// Now, the data must be validated. I used the functions declared in formvalidationtools.php and datatools.php
	// filling $error_message with text when something is wrong 
	$error_message = "";
	$error_message .= validateCharacters ( $name_first, 'De voornaam is niet valide.' );
	$error_message .= validateCharacters ( $name_last, 'De achternaam is niet valide.' );
	$error_message .= validateLength ( $phone, 1, 'Het telefoonnummer is niet ingevuld.' );
	$error_message .= validateEmail ( $email, 'Het email adres is niet valide' );
	if (emailExistsInMessage ( $email )) {
		$error_message .= 'Dit email adres bestaat al.<br/>';
	}
	$error_message .= validateLength ( $address_street, 1, 'De straatnaam is niet ingevuld.' );
	$error_message .= validateLength ( $address_number, 1, 'Het huisnummer is niet ingevuld.' );
	$error_message .= validateCharacters ( $address_city, 'De stad is niet ingevuld.' );
	$error_message .= validateLength ( $address_zipcode, 1, 'De postcode is niet ingevuld.' );
	$error_message .= validateLength ( $subject, 1, 'Het onderwerp van het bericht is niet ingevuld.' );
	$error_message .= validateLength ( $message, 10, 'Het bericht is te kort.' );
	// Validation has errors when the length of error_message > 0
	if (strlen ( $error_message ) > 0) {
		die ( $error_message );
	}
	
	$sql = "INSERT INTO `project`.`message` (`Name_first`, `Name_middle`, `Name_last`, `Email`, "
			."`Telephone`, `Adress_street`, `Adress_number`, `Adress_zipcode`, `Adress_city`, `Subject`, `Message`) "
			."VALUES ('$name_first', '$name_middle', '$name_last', '$email', '$phone', '$address_street', "
			."'$address_number', '$address_zipcode', '$address_city', '$subject', '$message');";
	if (! $mysqli->query ( $sql )) {
		die ( "Errormessage: ". $mysqli->error );
	}
}
?>
